
public class Niveles extends Arkanoid {
	//Max horizontal 13 bloques.

	/**
	 * A�ade todos los bloques y el fondo para el nivel 1.
	 */
	public static void mapa4() {
		//fondo
		f.background = "Fondos/Arkanoid.png";

		// ojo 1
		// row1
		bloques.add((new Bloque( 250, 100, 300, 120, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 300, 100, 350, 120, "bloques/redwall.png", 1)));

		// row2
		bloques.add((new Bloque( 200, 120, 250, 140, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 350, 120, 400, 140, "bloques/greenwall6.png", 1)));

		// row3
		bloques.add((new Bloque( 200, 140, 250, 160, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 350, 140, 400, 160, "bloques/greenwall6.png", 1)));

		// row4
		bloques.add((new Bloque( 200, 160, 250, 180, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 350, 160, 400, 180, "bloques/greenwall6.png", 1)));

		// row5
		bloques.add((new Bloque( 200, 180, 250, 200, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 350, 180, 400, 200, "bloques/greenwall6.png", 1)));

		// row6
		bloques.add((new Bloque( 250, 200, 300, 220, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 300, 200, 350, 220, "bloques/redwall.png", 1)));

		// ojo 2
		// row1
		bloques.add((new Bloque( 650, 100, 700, 120, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 700, 100, 750, 120, "bloques/redwall.png", 1)));

		// row2
		bloques.add((new Bloque( 600, 120, 650, 140, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 750, 120, 800, 140, "bloques/greenwall6.png", 1)));

		// row3
		bloques.add((new Bloque( 600, 140, 650, 160, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 750, 140, 800, 160, "bloques/greenwall6.png", 1)));

		// row4
		bloques.add((new Bloque( 600, 160, 650, 180, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 750, 160, 800, 180, "bloques/greenwall6.png", 1)));

		// row5
		bloques.add((new Bloque( 600, 180, 650, 200, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 750, 180, 800, 200, "bloques/greenwall6.png", 1)));

		// row6
		bloques.add((new Bloque( 650, 200, 700, 220, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 700, 200, 750, 220, "bloques/redwall.png", 1)));

		// boca // col 1
		bloques.add((new Bloque( 350, 300, 400, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 350, 320, 400, 340, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 350, 340, 400, 360, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 350, 360, 400, 380, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 350, 380, 400, 400, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 350, 400, 400, 420, "bloques/redwall.png", 1)));

		// col 2
		bloques.add((new Bloque( 500, 300, 550, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 500, 320, 550, 340, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 500, 340, 550, 360, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 500, 360, 550, 380, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 500, 380, 550, 400, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 500, 400, 550, 420, "bloques/redwall.png", 1)));

		// col 3
		bloques.add((new Bloque( 650, 300, 700, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 650, 320, 700, 340, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 650, 340, 700, 360, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 650, 360, 700, 380, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 650, 380, 700, 400, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 650, 400, 700, 420, "bloques/redwall.png", 1)));

		// row 1
		bloques.add((new Bloque( 400, 420, 450, 440, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 450, 420, 500, 440, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 550, 420, 600, 440, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 600, 420, 650, 440, "bloques/redwall.png", 1)));

	}

	/**
	 * A�ade todos los bloques y el fondo para el nivel 2.
	 */
	public static void mapa1() {
		// fondo
		f.background = "Fondos/Arkanoid2.png";

		// up
		bloques.add((new Bloque( 400, 180, 450, 200, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 550, 180, 600, 200, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 400, 200, 450, 220, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 550, 200, 600, 220, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 500, 240, 550, 260, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 450, 240, 500, 260, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 400, 220, 450, 240, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 550, 220, 600, 240, "bloques/greenwall6.png", 1)));

		// row1
		bloques.add((new Bloque( 200, 300, 250, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 250, 300, 300, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 300, 300, 350, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 350, 300, 400, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 600, 300, 650, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 650, 300, 700, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 700, 300, 750, 320, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 750, 300, 800, 320, "bloques/redwall.png", 1)));

		// row2
		bloques.add((new Bloque( 600, 320, 650, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 650, 320, 700, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 700, 320, 750, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 750, 320, 800, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 200, 320, 250, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 250, 320, 300, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 300, 320, 350, 340, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 350, 320, 400, 340, "bloques/greenwall6.png", 1)));

		// row3
		bloques.add((new Bloque( 250, 340, 300, 360, "bloques/yellowwall.png", 1)));
		bloques.add((new Bloque( 300, 340, 350, 360, "bloques/yellowwall.png", 1)));
		bloques.add((new Bloque( 350, 340, 400, 360, "bloques/yellowwall.png", 1)));
		bloques.add((new Bloque( 600, 340, 650, 360, "bloques/yellowwall.png", 1)));
		bloques.add((new Bloque( 650, 340, 700, 360, "bloques/yellowwall.png", 1)));
		bloques.add((new Bloque( 700, 340, 750, 360, "bloques/yellowwall.png", 1)));

		// row4
		bloques.add((new Bloque( 250, 360, 300, 380, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 300, 360, 350, 380, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 350, 360, 400, 380, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 600, 360, 650, 380, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 650, 360, 700, 380, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 700, 360, 750, 380, "bloques/orangewall.png", 1)));

		// row5
		bloques.add((new Bloque( 200, 380, 250, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 300, 380, 350, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 350, 380, 400, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 600, 380, 650, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 650, 380, 700, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 750, 380, 800, 400, "bloques/lightbluewall.png", 1)));

		// row6
		bloques.add((new Bloque( 200, 400, 250, 420, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 350, 400, 400, 420, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 600, 400, 650, 420, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 750, 400, 800, 420, "bloques/whitewall.png", 1)));

		// row7
		bloques.add((new Bloque( 200, 420, 250, 440, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 350, 420, 400, 440, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 600, 420, 650, 440, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 750, 420, 800, 440, "bloques/bluewall.png", 1)));

		// row8
		bloques.add((new Bloque( 350, 440, 400, 460, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 600, 440, 650, 460, "bloques/cyanwall.png", 1)));

		// row9
		bloques.add((new Bloque( 350, 460, 400, 480, "bloques/redwall.png", 1)));
		bloques.add((new Bloque( 600, 460, 650, 480, "bloques/redwall.png", 1)));

		// row10
		bloques.add((new Bloque( 350, 480, 400, 500, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 600, 480, 650, 500, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 750, 480, 800, 500, "bloques/ironwall.png", 999)));

		// row11
		bloques.add((new Bloque( 200, 480, 250, 500, "bloques/ironwall.png", 999)));
		bloques.add((new Bloque( 250, 500, 300, 520, "bloques/ironwall.png", 999)));
		bloques.add((new Bloque( 300, 500, 350, 520, "bloques/ironwall.png", 999)));
		bloques.add((new Bloque( 650, 500, 700, 520, "bloques/ironwall.png", 999)));
		bloques.add((new Bloque( 700, 500, 750, 520, "bloques/ironwall.png", 999)));

	}
	
	/**
	 * A�ade todos los bloques y el fondo para el nivel 3.
	 */
	public static void mapa2() {
		// fondo
		f.background = "Fondos/Arkanoid3.png";

		// row1
		bloques.add((new Bloque( 277, 180, 327, 200, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 677, 180, 727, 200, "bloques/greenwall6.png", 1)));
		
		// row2
		bloques.add((new Bloque( 327, 200, 377, 220, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 627, 200, 677, 220, "bloques/greenwall6.png", 1)));
		
		// row3
		bloques.add((new Bloque( 377, 220, 427, 240, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 577, 220, 627, 240, "bloques/greenwall6.png", 1)));
		
		// row4
		bloques.add((new Bloque( 427, 240, 477, 260, "bloques/ironwall.png", 999)));
		bloques.add((new Bloque( 527, 240, 577, 260, "bloques/ironwall.png", 999)));
		
		// row5
		bloques.add((new Bloque( 377, 260, 427, 280, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 427, 260, 477, 280, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 477, 260, 527, 280, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 527, 260, 577, 280, "bloques/bluewall.png", 1)));
		bloques.add((new Bloque( 577, 260, 627, 280, "bloques/bluewall.png", 1)));
		
		// row6
		bloques.add((new Bloque( 327, 280, 377, 300, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 377, 280, 427, 300, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 427, 280, 477, 300, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 477, 280, 527, 300, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 527, 280, 577, 300, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 577, 280, 627, 300, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 627, 280, 677, 300, "bloques/lightbluewall.png", 1)));
		
		// row7
		bloques.add((new Bloque( 327, 300, 377, 320, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 377, 300, 427, 320, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 427, 300, 477, 320, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 477, 300, 527, 320, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 527, 300, 577, 320, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 577, 300, 627, 320, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 627, 300, 677, 320, "bloques/lightbluewall.png", 1)));
		
		// row8
		bloques.add((new Bloque( 277, 320, 327, 340, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 327, 320, 377, 340, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 427, 320, 477, 340, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 477, 320, 527, 340, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 527, 320, 577, 340, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 627, 320, 677, 340, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 677, 320, 727, 340, "bloques/cyanwall.png", 1)));
		
		// row9
		bloques.add((new Bloque( 227, 340, 277, 360, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 277, 340, 327, 360, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 327, 340, 377, 360, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 377, 340, 427, 360, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 427, 340, 477, 360, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 477, 340, 527, 360, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 527, 340, 577, 360, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 577, 340, 627, 360, "bloques/whitewall.png", 1)));
		bloques.add((new Bloque( 627, 340, 677, 360, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 677, 340, 727, 360, "bloques/cyanwall.png", 1)));
		bloques.add((new Bloque( 727, 340, 777, 360, "bloques/greenwall6.png", 1)));
		
		// row10
		bloques.add((new Bloque( 227, 360, 277, 380, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 327, 360, 377, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 377, 360, 427, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 427, 360, 477, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 477, 360, 527, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 527, 360, 577, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 577, 360, 627, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 627, 360, 677, 380, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 727, 360, 777, 380, "bloques/greenwall6.png", 1)));

		// row11
		bloques.add((new Bloque( 227, 380, 277, 400, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 327, 380, 377, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 377, 380, 427, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 427, 380, 477, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 477, 380, 527, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 527, 380, 577, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 577, 380, 627, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 627, 380, 677, 400, "bloques/lightbluewall.png", 1)));
		bloques.add((new Bloque( 727, 380, 777, 400, "bloques/greenwall6.png", 1)));
		
		// row12
		bloques.add((new Bloque( 227, 400, 277, 420, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 327, 400, 377, 420, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 377, 400, 427, 420, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 577, 400, 627, 420, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 627, 400, 677, 420, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 727, 400, 777, 420, "bloques/greenwall6.png", 1)));
		
		// row13
		bloques.add((new Bloque( 227, 420, 277, 440, "bloques/greenwall6.png", 1)));
		bloques.add((new Bloque( 327, 420, 377, 440, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 627, 420, 677, 440, "bloques/orangewall.png", 1)));
		bloques.add((new Bloque( 727, 420, 777, 440, "bloques/greenwall6.png", 1)));
				
		// row14
		bloques.add((new Bloque( 377, 440, 427, 460, "bloques/ironwall3.png", 999)));
		bloques.add((new Bloque( 427, 440, 477, 460, "bloques/ironwall2.png", 999)));
		bloques.add((new Bloque( 527, 440, 577, 460, "bloques/ironwall3.png", 999)));
		bloques.add((new Bloque( 577, 440, 627, 460, "bloques/ironwall2.png", 999)));
	}
	
	/**
	 * A�ade todos los bloques y el fondo para el nivel 4.
	 */
	public static void mapa3() {
		//fondo
		f.background = "Fondos/Arkanoid4.png";
		
//		pruebas, ignorar
//		bloques.add((new Bloque( 177, 480, 227, 500, "bloques/ironwall.png", 1))); 1
//		bloques.add((new Bloque( 227, 480, 277, 500, "bloques/ironwall.png", 1))); 2
//		bloques.add((new Bloque( 277, 480, 327, 500, "bloques/ironwall.png", 1))); 3
//		bloques.add((new Bloque( 327, 480, 377, 500, "bloques/ironwall.png", 1))); 4
//		bloques.add((new Bloque( 377, 480, 427, 500, "bloques/ironwall.png", 1))); 5
//		bloques.add((new Bloque( 427, 480, 477, 500, "bloques/ironwall.png", 1))); 6
//		bloques.add((new Bloque( 477, 480, 527, 500, "bloques/ironwall.png", 1))); 7
//		bloques.add((new Bloque( 527, 480, 577, 500, "bloques/ironwall.png", 1))); 8
//		bloques.add((new Bloque( 577, 480, 627, 500, "bloques/ironwall.png", 1))); 9
//		bloques.add((new Bloque( 627, 480, 677, 500, "bloques/ironwall.png", 1))); 10
//		bloques.add((new Bloque( 677, 480, 727, 500, "bloques/ironwall.png", 1))); 11
//		bloques.add((new Bloque( 727, 480, 777, 500, "bloques/ironwall.png", 1))); 12
//		bloques.add((new Bloque( 777, 480, 827, 500, "bloques/ironwall.png", 1))); 13 
		
		//row 1
		bloques.add((new Bloque( 327, 100, 377, 120, "bloques/yellowwall.png", 1))); 
		bloques.add((new Bloque( 377, 100, 427, 120, "bloques/redwall.png", 1))); 
		bloques.add((new Bloque( 427, 100, 477, 120, "bloques/redwall.png", 1))); 
		
		//row 2
		bloques.add((new Bloque( 277, 120, 327, 140, "bloques/yellowwall.png", 1))); 
		bloques.add((new Bloque( 327, 120, 377, 140, "bloques/redwall.png", 1))); 
		bloques.add((new Bloque( 377, 120, 427, 140, "bloques/redwall.png", 1))); 
		bloques.add((new Bloque( 427, 120, 477, 140, "bloques/redwall.png", 1))); 
		
		//row 3
		bloques.add((new Bloque( 327, 140, 377, 160, "bloques/redwall.png", 1))); 
		bloques.add((new Bloque( 377, 140, 427, 160, "bloques/redwall.png", 1))); 
		bloques.add((new Bloque( 427, 140, 477, 160, "bloques/redwall.png", 1)));
		
		//row 4
		bloques.add((new Bloque( 277, 160, 327, 180, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 160, 377, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 160, 427, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 160, 477, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 160, 527, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 160, 577, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 160, 627, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 160, 677, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 160, 727, 180, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 160, 777, 180, "bloques/whitewall.png", 1))); 
		
		//row 5
		bloques.add((new Bloque( 227, 180, 277, 200, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 277, 180, 327, 200, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 180, 377, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 180, 427, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 180, 477, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 180, 527, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 180, 577, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 180, 627, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 180, 677, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 180, 727, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 180, 777, 200, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 777, 180, 827, 200, "bloques/whitewall.png", 1))); 
		
		//row 6
		bloques.add((new Bloque( 227, 200, 277, 220, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 277, 200, 327, 220, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 200, 377, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 200, 427, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 200, 477, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 200, 527, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 200, 577, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 200, 627, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 200, 677, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 200, 727, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 200, 777, 220, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 777, 200, 827, 220, "bloques/whitewall.png", 1))); 
		
		//row 7
		bloques.add((new Bloque( 227, 220, 277, 240, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 277, 220, 327, 240, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 220, 377, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 220, 427, 240, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 427, 220, 477, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 220, 527, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 220, 577, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 220, 627, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 220, 677, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 220, 727, 240, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 220, 777, 240, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 777, 220, 827, 240, "bloques/whitewall.png", 1))); 
		
		//row 8 
		bloques.add((new Bloque( 227, 240, 277, 260, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 277, 240, 327, 260, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 240, 377, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 240, 427, 260, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 427, 240, 477, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 240, 527, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 240, 577, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 240, 627, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 240, 677, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 240, 727, 260, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 240, 777, 260, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 777, 240, 827, 260, "bloques/whitewall.png", 1))); 
		
		//row 9
		bloques.add((new Bloque( 227, 260, 277, 280, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 277, 260, 327, 280, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 260, 377, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 260, 427, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 260, 477, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 260, 527, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 260, 577, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 260, 627, 280, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 627, 260, 677, 280, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 677, 260, 727, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 260, 777, 280, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 777, 260, 827, 280, "bloques/whitewall.png", 1))); 
		
		//row 10
		bloques.add((new Bloque( 227, 280, 277, 300, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 277, 280, 327, 300, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 280, 377, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 280, 427, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 280, 477, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 280, 527, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 280, 577, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 280, 627, 300, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 627, 280, 677, 300, "bloques/blackwall.png", 1))); 
		bloques.add((new Bloque( 677, 280, 727, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 280, 777, 300, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 777, 280, 827, 300, "bloques/whitewall.png", 1))); 
		
		//row 11
		bloques.add((new Bloque( 277, 300, 327, 320, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 300, 377, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 300, 427, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 300, 477, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 300, 527, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 300, 577, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 300, 627, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 300, 677, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 300, 727, 320, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 727, 300, 777, 320, "bloques/whitewall.png", 1))); 
		
		//row 12
		bloques.add((new Bloque( 377, 320, 427, 340, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 320, 477, 340, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 477, 320, 527, 340, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 320, 577, 340, "bloques/whitewall.png", 1))); 
		
		//row 13
		bloques.add((new Bloque( 377, 340, 427, 360, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 340, 477, 360, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 477, 340, 527, 360, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 340, 577, 360, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 340, 627, 360, "bloques/whitewall.png", 1)));
		
		//row 14
		bloques.add((new Bloque( 327, 360, 377, 380, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 377, 360, 427, 380, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 427, 360, 477, 380, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 360, 527, 380, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 360, 577, 380, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 360, 627, 380, "bloques/whitewall.png", 1))); 
		
		//row 15
		bloques.add((new Bloque( 277, 380, 327, 400, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 380, 377, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 380, 427, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 380, 477, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 380, 527, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 380, 577, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 380, 627, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 380, 677, 400, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 380, 727, 400, "bloques/lightgreywall.png", 1))); 
		
		//row 16
		bloques.add((new Bloque( 277, 400, 327, 420, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 327, 400, 377, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 377, 400, 427, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 427, 400, 477, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 477, 400, 527, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 400, 577, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 400, 627, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 627, 400, 677, 420, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 677, 400, 727, 420, "bloques/lightgreywall.png", 1))); 
		
		//row 17
		bloques.add((new Bloque( 377, 420, 427, 440, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 420, 477, 440, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 477, 420, 527, 440, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 420, 577, 440, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 420, 627, 440, "bloques/whitewall.png", 1)));
		
		//row 18
		bloques.add((new Bloque( 377, 440, 427, 460, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 440, 477, 460, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 477, 440, 527, 460, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 440, 577, 460, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 440, 627, 460, "bloques/whitewall.png", 1)));
		
		//row 19
		bloques.add((new Bloque( 377, 460, 427, 480, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 460, 477, 480, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 477, 460, 527, 480, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 460, 577, 480, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 460, 627, 480, "bloques/whitewall.png", 1)));
		
		//row 20
		bloques.add((new Bloque( 377, 480, 427, 500, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 480, 477, 500, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 477, 480, 527, 500, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 527, 480, 577, 500, "bloques/whitewall.png", 1))); 
		bloques.add((new Bloque( 577, 480, 627, 500, "bloques/whitewall.png", 1)));
		
		//row 21
		bloques.add((new Bloque( 377, 500, 427, 520, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 500, 477, 520, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 527, 500, 577, 520, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 577, 500, 627, 520, "bloques/lightgreywall.png", 1)));
		
		//row 22
		bloques.add((new Bloque( 377, 520, 427, 540, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 427, 520, 477, 540, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 527, 520, 577, 540, "bloques/lightgreywall.png", 1))); 
		bloques.add((new Bloque( 577, 520, 627, 540, "bloques/lightgreywall.png", 1)));
		
		//row 23
		bloques.add((new Bloque( 377, 540, 427, 560, "bloques/greywall.png", 1)));  
		bloques.add((new Bloque( 577, 540, 627, 560, "bloques/lightgreywall.png", 1)));
		
		//row 24
		bloques.add((new Bloque( 377, 560, 427, 580, "bloques/greywall.png", 1))); 
		bloques.add((new Bloque( 577, 560, 627, 580, "bloques/lightgreywall.png", 1)));
	}
	
}
