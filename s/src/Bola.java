import java.io.IOException;

public class Bola extends Moviles {
	/**
	 * Direccion en el eje y para el movimiento de la bola.
	 */
	char direcciony;
	/**
	 * Direccion en el eje x para el movimiento de la bola.
	 */
	char direccionx;
	/**
	 * Velocidad de la bola.
	 */
	double velocidad = 2;
	/**
	 * Estado de la bola.
	 */
	public int estado;

	public Bola(String name, int x1, int y1, int x2, int y2, String path, int estado) {
		super(name, x1, y1, x2, y2, path);
		this.estado = estado;

	}  

	/**
	 * Cambio de direccionde la bola al colisionar con los bloques.
	 */
	public void collbloques() {
		if (this.estado == 1) {
			if (direcciony == 't' && direccionx == 'f') {
				direccionx = 'h';
			} else if (direcciony == 't' && direccionx == 'h') {
				direcciony = 'b';
			} else if (direcciony == 'b' && direccionx == 'h') {
				direccionx = 'f';
			} else if (direcciony == 'b' && direccionx == 'f') {
				direcciony = 't';
			} else if (direcciony == 't' && direccionx == ' ') {
				direcciony = 'b';
			} else if (direcciony == 'b' && direccionx == ' ') {
				direcciony = 't';
			}
		} else if (this.estado == 2) {
//			for (Bloque b : Arkanoid.bloques) {
//				if (b.path == "bloques/ironwall.png" || b.path == "bloques/ironwall2.png" || b.path == "bloques/ironwall3.png") {
//					if (direcciony == 't' && direccionx == 'f') { 
//						direccionx = 'h';
//					} else if (direcciony == 't' && direccionx == 'h') {
//						direcciony = 'b';
//					} else if (direcciony == 'b' && direccionx == 'h') {
//						direccionx = 'f';
//					} else if (direcciony == 'b' && direccionx == 'f') {
//						direcciony = 't';
//					} else if (direcciony == 't' && direccionx == ' ') {
//						direcciony = 'b';
//					} else if (direcciony == 'b' && direccionx == ' ') {
//						direcciony = 't';
//					}
//				}
//			}
		}
	}

	/**
	 * Cambio de direccionde la bola al colisionar con los muros.
	 */
	public void collmuros() {
		if (direcciony == 't' && direccionx == 'f') {
			direccionx = 'h';
		} else if (direcciony == 't' && direccionx == 'h') {
			direcciony = 'b';
		} else if (direcciony == 'b' && direccionx == 'h') {
			direccionx = 'f';
		} else if (direcciony == 'b' && direccionx == 'f') {
			direcciony = 't';
		} else if (direcciony == 't' && direccionx == ' ') {
			direcciony = 'b';
		} else if (direcciony == 'b' && direccionx == ' ') {
			direcciony = 't';
		}
	}

	/**
	 * Colision de la Bola con el Sprite de la Nave Parte Izquierda.
	 */
	public void collstick1() {
		if (this.collidesWith(Arkanoid.n)) {
			Arkanoid.w.playSFX("Musica/Arkanoid2BolaStick.wav");
			direccionx = 'f';
			direcciony = 't';
		}
	}

	/**
	 * Colision de la Bola con el Sprite de la Nave Parte Central.
	 */
	public void collstick2() {
		if (Arkanoid.o.collidesWith(Arkanoid.n2)) {
			Arkanoid.w.playSFX("Musica/Arkanoid2BolaStick.wav");
			if (direcciony == 't' && direccionx == 'f') {
				direccionx = 'h';
			} else if (direcciony == 't' && direccionx == 'h') {
				direcciony = 'b';
			} else if (direcciony == 'b' && direccionx == 'h') {
				direccionx = 'f';
			} else if (direcciony == 'b' && direccionx == 'f') {
				direcciony = 't';
			} else if (direcciony == 't' && direccionx == ' ') {
				direcciony = 'b';
			} else if (direcciony == 'b' && direccionx == ' ') {
				direcciony = 't';
			}
		}
	}

	/**
	 * Colision de la Bola con el Sprite de la Nave Parte Derecha.
	 */
	public void collstick3() {
		if (this.collidesWith(Arkanoid.n3)) {
			Arkanoid.w.playSFX("Musica/Arkanoid2BolaStick.wav");
			direccionx = 'h';
			direcciony = 't';
		}
	}

	/**
	 * Cuando la bola llega al espacio asignado como vacio se elimina, se le quita
	 * una vida al jugador y se reseta la posicion de los objetos.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public void vacio() throws IOException {
		// "Colision" de la Bola con el vacio.
		if (Arkanoid.o.y1 >= 750) {
			Arkanoid.w.playSFX("Musica/Arkanoid2VidaMenos.wav");
			UI.getUI().hp--;
			Arkanoid.resetearposobjetos();
			if (UI.getUI().hp == 0) {
				Arkanoid.gameover();
			}

		}

	}

	/**
	 * Movimiento de la bola puesto en 4 direcciones principales 2 en y, 2 en x, y
	 * su combinacion hace las diagonales.
	 */
	public void movbola() {
		// t=arriba
		// b=abajo
		// f=izquierda
		// h=derecha
		if (Arkanoid.iniciarbola) {
			if (direcciony == 't') {
				y1 -= velocidad;
				y2 -= velocidad;

			} else if (direcciony == 'b') {
				y1 += velocidad;
				y2 += velocidad;
			}
			if (direccionx == 'f') {
				x1 -= velocidad;
				x2 -= velocidad;

			} else if (direccionx == 'h') {
				x1 += velocidad;
				x2 += velocidad;
			}

		}

	}

	/**
	 * Movimiento de la bola en la direccion que tiene desppues de colisionar para
	 * evitar problemas con las colisiones.
	 */

	public void movlibre() {
		if (Arkanoid.iniciarbola) {
			if (direcciony == 't') {
				y1 -= 3;
				y2 -= 3;

			} else if (direcciony == 'b') {
				y1 += 3;
				y2 += 3;
			}
			if (direccionx == 'f') {
				x1 -= 3;
				x2 -= 3;

			} else if (direccionx == 'h') {
				x1 += 3;
				x2 += 3;
			}

		}
	}
}
