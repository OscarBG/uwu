import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Powerup extends Sprite {

	/**
	 * Tipo del powerup.
	 */
	Enum tipo;
	
	public Powerup(int x1, int y1, String path, Enum tipo) {
		super("pu", x1, y1, x1 + 40, y1 + 20, path);
		this.tipo = tipo;
	}

	//TODO comprobar powerups, parar todos los timers cuando la bola choca con el vacio o se acaba el nivel.
	/**
	 * Colision del powerup con los sprites de la nave, comprueba su tipo y actua acorde al mismo, algunos usan un timer.
	 */
	public void powerups() {
		gravedad();
		if (collidesWith(Arkanoid.n) || collidesWith(Arkanoid.n2) || collidesWith(Arkanoid.n3)) {
			if (tipo == Tipo.MASVEL) {
				masvelocidad();
				timerpowerup(tipo);
			}
			if (tipo == Tipo.MENOSVEL) {
				menosvelocidad();
				timerpowerup(tipo);
			}
			if (tipo == Tipo.MENOSVIDA) {
				vidamenos();
			}
			if (tipo == Tipo.MASVIDA) {
				vidamas();
			}
			if (tipo == Tipo.DISPARO) {
				disparo();
			}
			if (tipo == Tipo.ATRAVESAR) {
				atravesar();
				timerpowerup(tipo);
			}
			if (tipo == Tipo.NAVE) {
				nave();
				timerpowerup(tipo);
			}
			delete();
		}
		if (this.y1 >= 750) {
			delete();
		}
	}

	/**
	 * Timer para la duracion de los powerups.
	 * @param tipo2 (Tipo del powerup)
	 */
	private void timerpowerup(Enum tipo2) {

		Timer timer = new Timer(" Timer");
		TimerTask t = new TimerTask() {

			public void run() {
				if (tipo2 == Tipo.MENOSVEL) {
					menosvelocidadreverse();
				}
				if (tipo2 == Tipo.MASVEL) {
					masvelocidadreverse();
				}
				if (tipo2 == Tipo.NAVE) {
					navereverse();
				}
				if (tipo2 == Tipo.ATRAVESAR) {
					atravesarreverse();
				}
			}
		};
		long d = 3500L;
		timer.schedule(t, d);

	}

	/**
	 * Movimiento aplicado a los powerups.
	 */
	public void gravedad() {
		y1++;
		y2++;
	}

	/**
	 * Aplica mas velocidad a la bola.
	 */
	public void masvelocidad() {
		Arkanoid.o.velocidad = 3;
	}

	/**
	 * Aplica menos velocidad a la bola, es el contrario de la funcion masvelocidad().
	 */
	public void masvelocidadreverse() {
		Arkanoid.o.velocidad = 2;
	}

	/**
	 * Aplica la menor velocidad posible a la bola.
	 */
	public void menosvelocidad() {
		Arkanoid.o.velocidad = 1;
	}

	/**
	 * Aplica la velocidad estandar a la bola.
	 */
	public void menosvelocidadreverse() {
		Arkanoid.o.velocidad = 2;
	}

	/**
	 * Resta una vida al jugador.
	 */
	public void vidamenos() {
		if (UI.getUI().hp > 1) {
			UI.getUI().hp--;
		}
	}

	/**
	 * A�ade una vida al jugador.
	 */
	public void vidamas() {
		UI.getUI().hp++;
	}

	/**
	 * Crea los proyectiles para el powerup de disparar.
	 */
	public void disparo() {
		Arkanoid.proyectiles.add(new Proyectil(Arkanoid.n.x1 - 5, Arkanoid.n.y1 - 30, Arkanoid.n.x1, Arkanoid.n.y1 - 5,
				"Utiles/disparo.png"));
		Arkanoid.proyectiles.add(new Proyectil(Arkanoid.n3.x2 + 5, Arkanoid.n3.y1 - 30, Arkanoid.n3.x2 + 10,
				Arkanoid.n3.y1 - 5, "Utiles/disparo.png"));
		Arkanoid.proyectiles.add(new Proyectil(Arkanoid.n.x1 - 15, Arkanoid.n.y1 - 30, Arkanoid.n.x1 - 10,
				Arkanoid.n.y1 - 5, "Utiles/disparo.png"));
		Arkanoid.proyectiles.add(new Proyectil(Arkanoid.n3.x2 + 15, Arkanoid.n3.y1 - 30, Arkanoid.n3.x2 + 20,
				Arkanoid.n3.y1 - 5, "Utiles/disparo.png"));
		Arkanoid.proyectiles.add(new Proyectil(Arkanoid.n.x1 - 25, Arkanoid.n.y1 - 30, Arkanoid.n.x1 - 20,
				Arkanoid.n.y1 - 5, "Utiles/disparo.png"));
		Arkanoid.proyectiles.add(new Proyectil(Arkanoid.n3.x2 + 25, Arkanoid.n3.y1 - 30, Arkanoid.n3.x2 + 30,
				Arkanoid.n3.y1 - 5, "Utiles/disparo.png"));
	}

	/**
	 * Sustituye los sprites de la nave para augmentar su longitud.
	 */
	public void nave() {
		int x1 = Arkanoid.n.x1;
		int x2 = Arkanoid.n.x2;
		Arkanoid.n.delete();
		Arkanoid.n2.delete();
		Arkanoid.n3.delete();
		Arkanoid.n = new Nave("Navei", x1, 722, x2, 750, "Utiles/Vausizq.gif");
		Arkanoid.n2 = new Nave("Navec", x2+1, 722, x2+130, 750, "Utiles/Vauscentrogrande.gif");
		Arkanoid.n3 = new Nave("Naved", x2+131, 722, x2+160, 750, "Utiles/Vausder.gif");
	}

	/**
	 * Devuelve los sprites de la nave a su estado base.
	 */
	public void navereverse() {
		int x1 = Arkanoid.n.x1;
		int x2 = Arkanoid.n.x2;
		Arkanoid.n.delete();
		Arkanoid.n2.delete();
		Arkanoid.n3.delete();
		Arkanoid.n = new Nave("Navei", x1, 722, x2, 750, "Utiles/Vausizq.gif");
		Arkanoid.n2 = new Nave("Navec", x2+1, 722, x2+90, 750, "Utiles/Vauscentro.gif");
		Arkanoid.n3 = new Nave("Naved", x2+91, 722, x2+120, 750, "Utiles/Vausder.gif");
	}
	
	/**
	 * Cambia el estado de la bola para que atraviese los bloques.
	 */
	public void atravesar() {
		Arkanoid.o.estado = 2;
	}
	
	/**
	 * Cambia la bola a su estado inicial.
	 */
	public void atravesarreverse() {
		Arkanoid.o.estado = 1;
	}
}
