import java.awt.Font;
import java.util.ArrayList;

public class UI {
	/**
	 * Textos mostrados in game en el nivel.
	 */
	Font fuente = new Font("Monospaced", Font.BOLD, 30);
	int score;
	int hp;
	String hptext = "";
	String puntuaciontext = "";
	TextosMenu t = new TextosMenu( 200, 770, 225, 790, hptext, fuente, 0xF4FA58);
	TextosMenu t2 = new TextosMenu( 420, 20, 470, 30, puntuaciontext, fuente, 0xF4FA58);
	Sprite Vidas = new Sprite("", 140, 770, 195, 790, "UI/Vidas.png");
	ArrayList<Sprite> lista = new ArrayList<Sprite>();
	ArrayList<Paredes> paredes = new ArrayList<>();
	boolean vida1 = false;
	boolean vida2 = false;

	/**
	 * Singleton de los sprites y textos que van a estar siempre en el nivel.
	 */
	private UI() {
		score = 0;
		hp = 3;
		lista.add(t);
		lista.add(t2);
		lista.add(Vidas);
		paredes.add((new Paredes("pared1", 150, 075, 175, 765, "UI/ColumnaIzquierda.png")));
		paredes.add((new Paredes("pared2", 825, 075, 850, 765, "UI/ColumnaDerecha.png")));
		paredes.add((new Paredes("techo", 150, 050, 850, 075, "UI/Techo.png")));
	}

	private static UI ui;
	
	public static UI getUI() {
		if (ui == null) {
			ui = new UI();
			return ui;
		} else {
			return ui;
		}

	}

	public void textosnivel() {
		hptext = "x" + Integer.toString(hp);
		puntuaciontext = "Score: " + Integer.toString(score);
		t2.path = puntuaciontext;
		t.path = hptext;

		if (score == 3000 && vida1 == false) {
			vida1 = true;
			hp++;
		}
		if (score == 6000 && vida2 == false) {
			vida2 = true;
			hp++;
		}

	}
}