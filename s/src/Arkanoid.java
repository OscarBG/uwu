
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;

/**
 * El juego del Arkanoid.
 * 
 * @author Oscar Bravo
 * @version 14.0
 *
 */
public class Arkanoid {

	/**
	 * Inicializaciones varias (nave, vacio, bola, etc). La nave esta inicializada
	 * con 1 pixel de pormedio entre cada sprite para que no de fallos, si los
	 * sprites se superponen suele hacer cosas raras.
	 */
	static Field f = new Field();
	static Window w = new Window(f);

	static Nave n = new Nave("Navei", 400, 722, 429, 750, "Utiles/Vausizq.gif");
	static Nave n2 = new Nave("Navec", 430, 722, 519, 750, "Utiles/Vauscentro.gif");
	static Nave n3 = new Nave("Naved", 520, 722, 549, 750, "Utiles/Vausder.gif");
	static Bola o = new Bola("Bola", 470, 708, 480, 718, "Utiles/Ball.gif", 1);

	static ArrayList<Bloque> bloques = new ArrayList<>();
	static ArrayList<Powerup> powerups = new ArrayList<>();
	static ArrayList<Proyectil> proyectiles = new ArrayList<>();
	static ArrayList<Puntuacion> ranking = new ArrayList<Puntuacion>();

	static boolean iniciarbola = false;
	static boolean salir1 = false;

	static int lvl = 1;
	static int lvlbloques = 0;

	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		MostrarRanking();
		// Musica del menu.
		w.playMusic("Musica/Arkanoid2MenuTheme.wav");
		// Menu de seleccion.
		menu();

		boolean salir2 = false;
		while (!salir2) {
			// Cambio de niveles.
			init(lvl);

			while (!salir1) {
				// teclas que pulsa el jugador.
				inputjugador();
				// movimiento de la bola usando las direcciones.
				o.movbola();
				// Poner sprites y drawearlos.
				ArrayList<Sprite> sprites = new ArrayList<Sprite>();
				sprites.add(n);
				sprites.add(n2);
				sprites.add(n3);
				sprites.add(o);
				sprites.addAll(UI.getUI().paredes);
				sprites.addAll(UI.getUI().lista);
				sprites.add(UI.getUI().Vidas);
				sprites.addAll(bloques);
				sprites.addAll(powerups);
				sprites.addAll(proyectiles);
				f.draw(sprites);

				// Iteradores para eliminar los sprites al momento y que el programa no consuma
				// recursos de mas y no vaya lento.
				for (Iterator iterator = sprites.iterator(); iterator.hasNext();) {
					Sprite s = (Sprite) iterator.next();
					if (s.delete) {
						iterator.remove();
					}

				}
				for (Iterator iterator = bloques.iterator(); iterator.hasNext();) {
					Bloques b = (Bloques) iterator.next();
					if (b.delete) {
						iterator.remove();
					}

				}
				for (Iterator iterator = powerups.iterator(); iterator.hasNext();) {
					Powerup p = (Powerup) iterator.next();
					if (p.delete) {
						iterator.remove();
					}

				}
				for (Iterator iterator = proyectiles.iterator(); iterator.hasNext();) {
					Proyectil pr = (Proyectil) iterator.next();
					if (pr.delete) {
						iterator.remove();
					}

				}
				// Colision bola-stick sprite 1.
				o.collstick1();
				// Colision bola-stick sprite 2.
				o.collstick2();
				// Colision bola-stick sprite 3.
				o.collstick3();
				// Textos que se muestran dentro del nivel.
				UI.getUI().textosnivel();
				// Colision bola-bloques
				collbola();
				// Powerups.
				powerup();
				// Movimiento de los proyectiles si hay.
				proyectil();
				// Comprovar el final del nivel y el juego.
				comprovarfinal();

				if (w.getPressedKeys().contains('p')) {
					pause();
					break;
				}
				Thread.sleep(5);
				/*
				 * System.out.println("lvlbloques " +lvlbloques);
				 * System.out.println("proyectiles " +proyectiles.size());
				 * System.out.println("bloques " +bloques.size());
				 * System.out.println("powerups " +powerups.size());
				 * System.out.println("Sprites " +sprites.size());
				 * System.out.println(o.velocidad);
				 */

			}
		}
	}

	/**
	 * Funcion para los proyectiles, si es que hay.
	 */
	public static void proyectil() {
		for (Proyectil p : proyectiles) {
			p.move(f);
		}
	}

	/**
	 * Funcion para los powerups, si es que hay.
	 */
	public static void powerup() {
		for (Powerup p : powerups) {
			p.powerups();
		}
	}

	/**
	 * Colisiones de la bola con bloques y muros.
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 */
	public static void collbola() throws IOException, ClassNotFoundException {
		for (Bloque bloque : bloques) {
			if (o.collidesWith(bloque)) {
				w.playSFX("Musica/Arkanoid2BolaBloque.wav");
				// Cambio de la direccion de la bola cuando colisiona con los bloques.
				o.collbloques();
				// Si la bola colisiona con un bloque y este no tiene vida se elimina.
				bloque.deleteBloque();
				// Movimiento de la bola al colisionar, se aleja en la direccion que tiene
				// asignada.
				o.movlibre();
			}
		}

		// Colision bola-muros.
		for (Paredes tierra : UI.getUI().paredes) {
			if (o.collidesWith(tierra)) {
				w.playSFX("Musica/Arkanoid2BolaStick.wav");
				o.collmuros();
			}
		}
		// Bola llega al vacio, se elimina.
		o.vacio();
	}

	/**
	 * Resetea la posicion de la bola y la nave cuando la bola colisiona con el
	 * vacio o se cambia de nivel.
	 */
	public static void resetearposobjetos() {
		o.delete();
		n.delete();
		n2.delete();
		n3.delete();
		n = new Nave("Navei", 400, 722, 429, 750, "Utiles/Vausizq.gif");
		n2 = new Nave("Navec", 430, 722, 519, 750, "Utiles/Vauscentro.gif");
		n3 = new Nave("Naved", 520, 722, 549, 750, "Utiles/Vausder.gif");
		o = new Bola("Bola", 470, 708, 480, 718, "Utiles/Ball.gif", 1);
		iniciarbola = false;
		proyectiles.clear();
		powerups.clear();
	}

	/**
	 * Final del juego, el jugador gana la partida.
	 */
	public static void comprovarfinal() {
		int maxlvl = 4;
		// System.out.println(lvlbloques);
		if (lvlbloques == 0) {
			lvl++;
			resetearposobjetos();
			bloques.clear();
			// System.out.println(lvl);
			salir1 = true;
		}

		if (lvl > maxlvl) {
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			Gifs win = new Gifs("", 0, 0, 1024, 850, "Menus/win.gif");
			sprites.add(win);
			w.playMusic("Musica/Arkanoid2Win.wav");
			f.draw(sprites);
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.exit(0);
		}
	}

	/**
	 * Final del juego, el jugador pierde la partida.
	 * 
	 * @throws IOException
	 */
	public static void gameover() throws IOException {

		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		Gifs gameover = new Gifs("", 0, 0, 980, 880, "Menus/gameover.gif");
		sprites.add(gameover);
		w.playMusic("Musica/Arkanoid2Ending.wav");
		f.draw(sprites);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		EscribirRanking();
		System.exit(0);
	}

	/**
	 * Menu principal del juego.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public static void menu() throws IOException, ClassNotFoundException, InterruptedException {
		boolean jugar = false;
		while (!jugar) {
			// Fondo del menu.
			f.background = "Fondos/Principal.png";

			// Textos del menu.
			Font fuente2 = new Font("Monospaced", Font.BOLD, 40);
			TextosMenu tm1 = new TextosMenu(90, 595, 190, 610, "iniciar", fuente2, 0xFFFFFF);
			TextosMenu tm2 = new TextosMenu(110, 770, 210, 795, "mover", fuente2, 0xFFFFFF);
			TextosMenu tm3 = new TextosMenu(110, 405, 210, 430, "salir", fuente2, 0xFFFFFF);
			TextosMenu tm4 = new TextosMenu(60, 170, 90, 270, "controles", fuente2, 0xFFFFFF);
			String str = "Iniciar";
			String str2 = "Mover";
			String str3 = "Salir";
			String str4 = "Controles:";
			tm1.path = str;
			tm2.path = str2;
			tm3.path = str3;
			tm4.path = str4;

			// Uso del raton en el menu.
			int x = f.getMouseX();
			int y = f.getMouseY();
			int rx = f.getRightMouseX();
			int ry = f.getRightMouseY();

			// Sprites del menu.
			ArrayList<Sprite> sprites = new ArrayList<>();
			Gifs g = new Gifs("", 40, 660, 300, 760, "Menus/teclas.gif");
			Gifs g2 = new Gifs("", 40, 475, 300, 575, "Menus/space.gif");
			Gifs g3 = new Gifs("", 80, 295, 260, 395, "Menus/tab.gif");
			Gifs g4 = new Gifs("", 450, 600, 980, 830, "Menus/Ship.gif");
			Opciones b = new Opciones(430, 200, 700, 300, "Menus/newgame.png");
			Opciones b2 = new Opciones(430, 320, 700, 420, "Menus/continue.png");
			Opciones b3 = new Opciones(430, 440, 700, 540, "Menus/levels.png");
			Opciones b4 = new Opciones(430, 560, 700, 660, "Menus/ranking.png");
			sprites.add(g);
			sprites.add(g2);
			sprites.add(g3);
			sprites.add(g4);
			sprites.add(b);
			sprites.add(b2);
			sprites.add(b3);
			sprites.add(b4);
			sprites.add(tm1);
			sprites.add(tm2);
			sprites.add(tm3);
			sprites.add(tm4);
			f.draw(sprites);

			// Colision del raton con el sprite correspondiente segun su path.
			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y) || s.collidesWithPoint(rx, ry)) {
					if (s.path == "Menus/newgame.png") {
						jugar = true;
					}
					if (s.path == "Menus/continue.png") {
						cargar();
						jugar = true;
					}
					if (s.path == "Menus/levels.png") {
						lvlselector();
					}
					if (s.path == "Menus/ranking.png") {
						ranking();
					}
				}
			}

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void ranking() throws IOException, ClassNotFoundException, InterruptedException {
		boolean rankingb = false;
		while (!rankingb) {
			
			// Fondo del ranking.
			f.background = "Fondos/Principal.png";
			// Textos del ranking.
			Font fuente2 = new Font("Monospaced", Font.BOLD, 40);
			String name = "Name";
			String score = "Score";
			String lvl = "Level";
			String puntos = ranking.get(0).nombre + ":   " + ranking.get(0).score + " points   " + ranking.get(0).lvl;
			String puntos2 = ranking.get(1).nombre + ":   " + ranking.get(1).score + " points   " + ranking.get(1).lvl;
			String puntos3 = ranking.get(2).nombre + ":   " + ranking.get(2).score + " points   " + ranking.get(2).lvl;
			String puntos4 = ranking.get(3).nombre + ":   " + ranking.get(3).score + " points   " + ranking.get(3).lvl;
			String puntos5 = ranking.get(4).nombre + ":   " + ranking.get(4).score + " points   " + ranking.get(4).lvl;
			TextosMenu r1 = new TextosMenu(280, 150, 700, 250, "", fuente2, 0xFFFFFF);
			TextosMenu r2 = new TextosMenu(280, 250, 700, 350, "", fuente2, 0xFFFFFF);
			TextosMenu r3 = new TextosMenu(280, 350, 700, 450, "", fuente2, 0xFFFFFF);
			TextosMenu r4 = new TextosMenu(280, 450, 700, 550, "", fuente2, 0xFFFFFF);
			TextosMenu r5 = new TextosMenu(280, 550, 700, 650, "", fuente2, 0xFFFFFF);
			TextosMenu namet = new TextosMenu(280, 130, 700, 200, "", fuente2, 0xFFFFFF);
			TextosMenu scoret = new TextosMenu(498, 130, 800, 200, "", fuente2, 0xFFFFFF);
			TextosMenu lvlt = new TextosMenu(735, 130, 800, 200, "", fuente2, 0xFFFFFF);
			r1.path = puntos;
			r2.path = puntos2;
			r3.path = puntos3;
			r4.path = puntos4;
			r5.path = puntos5;
			namet.path = name;
			scoret.path = score;
			lvlt.path = lvl;
			Opciones b5 = new Opciones(800, 700, 1000, 800, "Menus/back.png");

			// Uso del raton en el Ranking.
			int x = f.getMouseX();
			int y = f.getMouseY();

			ArrayList<Sprite> sprites = new ArrayList<>();

			sprites.add(r1);
			sprites.add(r2);
			sprites.add(r3);
			sprites.add(r4);
			sprites.add(r5);
			sprites.add(namet);
			sprites.add(scoret);
			sprites.add(lvlt);
			sprites.add(b5);
			f.draw(sprites);

			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "Menus/back.png") {
						rankingb = true;
					}
				}
			}

			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Menu del seleccionador de nivel.
	 */
	public static void lvlselector() {
		boolean selector = false;
		while (!selector) {

			// Fondo del seleccionador de nivel.
			f.background = "Fondos/Principal.png";

			// Textos del seleccionador de nivel.
			Font fuente2 = new Font("Monospaced", Font.BOLD, 40);
			TextosMenu tls1 = new TextosMenu(250, 200, 500, 225, "Nivel:", fuente2, 0xFFFFFF);
			TextosMenu tls2 = new TextosMenu(110, 250, 210, 275, "Nivel 1", fuente2, 0xFFFFFF);
			TextosMenu tls3 = new TextosMenu(410, 250, 510, 275, "Nivel 2", fuente2, 0xFFFFFF);
			TextosMenu tls4 = new TextosMenu(710, 250, 810, 275, "Nivel 3", fuente2, 0xFFFFFF);
			TextosMenu tls5 = new TextosMenu(110, 450, 210, 500, "Nivel 4", fuente2, 0xFFFFFF);
			String str = "Select level:";
			String str2 = "Level 1";
			String str3 = "Level 2";
			String str4 = "Level 3";
			String str5 = "Level 4";
			tls1.path = str;
			tls2.path = str2;
			tls3.path = str3;
			tls4.path = str4;
			tls5.path = str5;

			// Uso del raton en el seleccionador de nivel.
			int x = f.getMouseX();
			int y = f.getMouseY();

			// Sprites del seleccionador de nivel.
			Opciones lvl1 = new Opciones(110, 290, 280, 460, "Imagenes/Nivel1.png");
			Opciones lvl2 = new Opciones(410, 290, 580, 460, "Imagenes/Nivel2.png");
			Opciones lvl3 = new Opciones(710, 290, 880, 460, "Imagenes/Nivel3.png");
			Opciones lvl4 = new Opciones(110, 510, 280, 710, "Imagenes/Nivel4.png");
			Opciones b5 = new Opciones(800, 700, 1000, 800, "Menus/back.png");
			ArrayList<Sprite> sprites = new ArrayList<>();
			sprites.add(b5);
			sprites.add(lvl1);
			sprites.add(lvl2);
			sprites.add(lvl3);
			sprites.add(lvl4);
			sprites.add(tls1);
			sprites.add(tls2);
			sprites.add(tls3);
			sprites.add(tls4);
			sprites.add(tls5);
			f.draw(sprites);

			// Colision del raton con el sprite correspondiente segun su path.
			for (Sprite s : sprites) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "Imagenes/Nivel1.png") {
						lvl = 1;
						selector = true;
					}
					if (s.path == "Imagenes/Nivel2.png") {
						lvl = 2;
						selector = true;
					}
					if (s.path == "Imagenes/Nivel3.png") {
						lvl = 3;
						selector = true;
					}
					if (s.path == "Imagenes/Nivel4.png") {
						lvl = 4;
						selector = true;
					}
					if (s.path == "Menus/back.png") {
						selector = true;
					}
				}
			}

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Inicializacion del nivel.
	 * 
	 * @param lvl2 (se le pasa el nivel en el cual esta).
	 */
	public static void init(int lvl2) {
		if (lvl == 1) {
			// Musica del nivel.
			w.playMusic("Musica/Arkanoid2LevelTheme.wav");
			// Nivel 1.
			Niveles.mapa1();
			salir1 = false;
			comprobarpathbloques();
		} else if (lvl == 2) {
			// Nivel 2
			Niveles.mapa2();
			// Animacion del cambio de nivel.
			Gifs g1 = new Gifs("", 0, 0, 1024, 880, "Menus/Transicion.gif");
			f.draw(g1);
			// Musica del nivel.
			w.playMusic("Musica/Arkanoid2LevelTheme.wav");
			salir1 = false;
			comprobarpathbloques();
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		} else if (lvl == 3) {
			// Nivel 3
			Niveles.mapa3();
			// Animacion del cambio de nivel.
			Gifs g1 = new Gifs("", 0, 0, 1024, 880, "Menus/Transicion.gif");
			f.draw(g1);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// Musica del nivel.
			w.playMusic("Musica/Arkanoid2LevelTheme.wav");
			salir1 = false;
			comprobarpathbloques();
		} else if (lvl == 4) {
			// Nivel 4
			Niveles.mapa4();
			// Animacion del cambio de nivel.
			Gifs g1 = new Gifs("", 0, 0, 1024, 880, "Menus/Transicion.gif");
			f.draw(g1);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// Musica del nivel.
			w.playMusic("Musica/Arkanoid2LevelTheme.wav");
			salir1 = false;
			comprobarpathbloques();
		}

	}

	/**
	 * Comprueba el path del bloque, para diferenciar los bloques normales de los
	 * bloques irrompibles, si son irrompibles no se suman a la variable lvlbloques.
	 */
	public static void comprobarpathbloques() {
		for (Bloque bloque : bloques) {
			if (bloque.path == "bloques/ironwall.png" || bloque.path == "bloques/ironwall.png"
					|| bloque.path == "bloques/ironwall.png") {
				// No hace nada.
			} else {
				lvlbloques++;
			}
		}
	}

	/**
	 * Teclas que el jugador pulsa y tienen una accion asignada.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException
	 */
	public static void inputjugador() throws IOException, ClassNotFoundException, InterruptedException {

		// Salir del juego tecla TAB
		if (w.getPressedKeys().contains('\t')) {
			System.exit(0);
			//System.out.println("salir");
		}
		// Iniciar el movimiento de la bola.
		if (w.getPressedKeys().contains(' ')) {
			if (iniciarbola == false) {
				iniciarbola = true;
				o.direcciony = 't';
				o.direccionx = 'f';
			}
		}
		// Movimiento hacia la izquierda del stick.
		if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('A')) {
			if (iniciarbola == false) {
				movimientobasestickybolaizq();
				o.x1 -= 3;
				o.x2 -= 3;
			} else if (iniciarbola == true) {
				movimientobasestickybolaizq();
			}
			// Movimiento hacia la derecha del stick.
		} else if (w.getPressedKeys().contains('d') || w.getPressedKeys().contains('D')) {
			if (iniciarbola == false) {
				movimientobasestickybolader();
				o.x1 += 3;
				o.x2 += 3;

			} else if (iniciarbola == true) {
				movimientobasestickybolader();
			}
		}
	}

	private static void pause() throws IOException, ClassNotFoundException, InterruptedException {
		boolean salir = false;
		while (!salir) {
			f.background = "Fondos/Principal.png";

			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			ArrayList<Sprite> sp = new ArrayList<Sprite>();
			Sprite continuar = new Sprite("", 300, 200, 700, 350, "Menus/resume.png");
			Sprite guardar = new Sprite("", 300, 400, 700, 550, "Menus/save.png");
			Sprite salirnivel = new Sprite("", 300, 600, 700, 750, "Menus/exitgame.png");
			sp.add(guardar);
			sp.add(salirnivel);
			sp.add(continuar);
			f.draw(sp);
			for (Sprite s : sp) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "Menus/save.png") {
						guardar();
						salir = true;

					}
					if (s.path == "Menus/exitgame.png") {
						System.exit(0);

					}
					if (s.path == "Menus/resume.png") {
						salir = true;
					}

				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private static void movimientobasestickybolader() {
		n.moveDer();
		n2.moveDer();
		n3.moveDer();
		boolean quieto = false;
		for (Paredes tierra : UI.getUI().paredes) {
			if (n3.collidesWith(tierra)) {
				if (iniciarbola == true) {
					restarx();
				} else {
					quieto = true;
					restarx();
				}
			}
		}
		if (quieto == true) {
			o.x1 -= 3;
			o.x2 -= 3;
		}

	}

	public static void movimientobasestickybolaizq() {
		n.moveIzq();
		n2.moveIzq();
		n3.moveIzq();
		boolean quieto = false;
		for (Paredes tierra : UI.getUI().paredes) {
			if (n.collidesWith(tierra)) {
				if (iniciarbola == true) {
					sumarx();
				} else {
					quieto = true;
					sumarx();
				}
			}
		}
		if (quieto == true) {
			o.x1 += 3;
			o.x2 += 3;
		}

	}

	/**
	 * Resta de x1 y x2 para los sprites del stick, para agilizar el movimiento.
	 */
	public static void restarx() {
		n.x1 -= 3;
		n.x2 -= 3;
		n2.x1 -= 3;
		n2.x2 -= 3;
		n3.x1 -= 3;
		n3.x2 -= 3;
	}

	/**
	 * Suma de x1 y x2 para los sprites del stick, para agilizar el movimiento.
	 */
	public static void sumarx() {
		n.x1 += 3;
		n.x2 += 3;
		n2.x1 += 3;
		n2.x2 += 3;
		n3.x1 += 3;
		n3.x2 += 3;
	}
	//TODO arreglar
	public static void EscribirRanking() throws IOException {
		String nombre = w.showInputPopup("Escribre tu nombre: ");
		ranking.add(new Puntuacion(nombre, UI.getUI().score, lvl));
		File f2 = new File("Datos/ranking.txt");
		FileWriter out = new FileWriter(f2, true);
		BufferedWriter bw = new BufferedWriter(out);
		if (!f2.exists()) {
			f2.createNewFile();
		}
		for (Puntuacion r : ranking) {
			bw.write(r.nombre + "," + r.score + "," + r.lvl);
			bw.newLine();
		}
		//Collections.sort(ranking);
		bw.flush();
		bw.close();
	}


	public static void MostrarRanking() throws IOException {
		File f = new File("Datos/ranking.txt");
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		if (!f.exists()) {
			f.createNewFile();
		}
		while (br.ready()) {
			String s = br.readLine();
			String[] spl = s.split(",");
			Puntuacion r = new Puntuacion(spl[0], Integer.parseInt(spl[1]), Integer.parseInt(spl[2]));
			ranking.add(r);
		}
		br.close();
	}

	/**
	 * Carga los valores guardados de la partida.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	private static void cargar() throws IOException, ClassNotFoundException, InterruptedException {
		File f1 = new File("Datos/guardar.txt");
		if (!f1.exists()) {
			f1.createNewFile();
		}
		FileInputStream fis = new FileInputStream(f1);
		ObjectInputStream ois = new ObjectInputStream(fis);

		UI.getUI().score = (int) ois.readObject();
		Arkanoid.lvl = (int) ois.readObject();
		UI.getUI().hp = (int) ois.readObject();
		ois.close();

	}

	/**
	 * Guarda los valores de la partida.
	 * @throws IOException
	 */
	public static void guardar() throws IOException {
		File f = new File("Datos/guardar.txt");
		if (f.exists())
			f.delete();
		f.createNewFile();
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(UI.getUI().score);
		oos.writeObject(Arkanoid.lvl);
		oos.writeObject(UI.getUI().hp);
		oos.flush();
		oos.close();
	}
}