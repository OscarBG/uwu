
public class Proyectil extends Sprite {

	public Proyectil(int x1, int y1, int x2, int y2, String path) {
		super("pro", x1, y1, x2, y2, path);

	}

	/**
	 * Movimiento del proyectil.
	 * @param f (El field)
	 */
	public void move(Field f) {
		y1 -= 2;
		y2 -= 2;
		coll(f);
	}

	/**
	 * Colisiones del proyectil.
	 * @param f (El field)
	 */
	private void coll(Field f) {
		for (Sprite s : collidesWithField(f)) {
			if (s != null) {
				if (s instanceof Bloques) {
					if (s.path=="bloques/ironwall.png" || s.path=="bloques/ironwall2.png" || s.path=="bloques/ironwall3.png") {
						
					}else {
					s.delete();
					Arkanoid.lvlbloques--;	
					}
					this.delete();
				}
				if (s instanceof Muros) {
					this.delete();
				}
			}
		}
	}
}
