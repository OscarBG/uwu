
public class Nave extends Moviles {

	/**
	 * Direccion asignada al movimiento de la nave.
	 */
	char direccion = 'd';

	public Nave(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.terrain = true;
	}
	
	/**
	 * Movimiento hacia la izquierda de la nave.
	 */
	public void moveIzq() {
		x1 = x1 - 3;
		x2 = x2 - 3;
		direccion = 'i';
	}

	/**
	 * Movimiento hacia la derecha de la nave.
	 */
	public void moveDer() {
		x1 = x1 + 3;
		x2 = x2 + 3;
		direccion = 'd';
	}


}
