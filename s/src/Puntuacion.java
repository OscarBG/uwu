import java.util.Date;

public class Puntuacion implements Comparable{
	
	/**
	 * Variable que define el nombre del jugador en el ranking
	 */
	String nombre;
	/**
	 * Variable que define los puntos del jugador en el ranking
	 */
	int score;
	int lvl;
	String fecha;
	public Puntuacion(String nombre, int score, int lvl) {
		super();
		this.nombre = nombre;
		this.score = score;
		this.lvl = lvl;
	}
	
	@Override
	public String toString() {
		return "Puntuacion [nombre=" + nombre + ", puntos=" + score + ", niveles=" + lvl + "]";
	}

	@Override
	public int compareTo(Object p) {
		{
			Puntuacion otro = (Puntuacion) p;
			if(otro.score < this.score)
			{
				return -1;
			}
			else if(otro.score == this.score)
			{
				return 0;
			}
			else 
			{
				return 1;
			}		
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + score;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Puntuacion other = (Puntuacion) obj;
		if (score != other.score)
			return false;
		return true;
	}
	
	

	

}
