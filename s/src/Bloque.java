import java.util.Random;

public class Bloque extends Bloques {
	/**
	 * Vida del bloque.
	 */
	int vida;

	// TODO String color;
	public Bloque(int x1, int y1, int x2, int y2, String path, int v) {
		super("bl", x1, y1, x2, y2, path);
		terrain = true;
		vida = v;
	}

	/**
	 * Comprueba que la bola colisiona con el bloque y si lo hace le baja la vida,
	 * si no tiene vida lo deletea y puede hacer aparecer un powerup.
	 */
	public void deleteBloque() {
		vida--;
		if (vida == 0) {
			delete();
			UI.getUI().score += 100;
			Arkanoid.lvlbloques--;// ESTO ES CUTRE
			Random r = new Random();
			if (r.nextInt(20) == 2) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupS.gif", Tipo.MASVEL));
			}
			if (r.nextInt(20) == 8) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupB.gif", Tipo.MENOSVEL));
			}
			if (r.nextInt(20) == 9) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupL.gif", Tipo.MASVIDA));
			}
			if (r.nextInt(20) == 7) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupE.gif", Tipo.DISPARO));
			}
			if (r.nextInt(20) == 14) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupP.gif", Tipo.MENOSVIDA));
			}
			if (r.nextInt(20) == 4) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupC.gif", Tipo.NAVE));
			}
			if (r.nextInt(20) == 6) {
				Arkanoid.powerups.add(new Powerup(x1 + 10, y1, "PW/PowerupD.gif", Tipo.ATRAVESAR));
			}
		}

	}
}
